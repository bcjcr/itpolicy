Balliol College JCR
---
We live in a digital age
---
And are spending more and more time online, 
so it is more important than ever to revaluate 
the services we are using
---
Our immediate assesment has identified two large areas
of improvement. 
Our use of facebook and office 365.
---
Our usage of facebook is directly supporting this
questionable company mired in scandel.
--- 
Facebook fails to protect our privacy, consider the cambridge analytica scandal.

It Creates an environment for the incitement of hate, 
consider the buddhist anti-muslim extremists in Sri Lanka,
where the facebook algorithm actively promoted anti-islamic posts.

And note the fact that the UN has directly condemmed facebook for the
incitement to genocide in Myanmar.
---
Closer to home it entrenches dangerous stereotypes by showing
target job adverts to only certain groups. 

and also provides a space for for conspiracy theorists and misogynistic
groups and generally spreads hate and dangerous misinformation.
---

We cannot be morally complicit in these impacts by further supporting it

---
Fortunatley we have succesfully identified alternatives.
[Matrix] provides an alternative to facebook messenger.

And Mastodon is an alternative to the other facebook functionalities.

Of these we will be able to implement [Matrix] quickly.
---
[Matrix] provides end to end encryption and we would be fully in 
control as is could be hosted here in balliol.
---
We would then also be able to talk to the larger net of other 
universities moving to Matrix. And further supporting its
development, and users. 
---
It is open-source, decentralised and not owned by any company. 
The computer science department already has a server runninng.
So matrix would be easy to implement.
Mastodon will take slightly longer however.
---
We are still exploring alternatives to the other services provided.
To ensure sustainable and ethical IT provision for the JCR.
---
Thank you for watching. Find the associated, accesible document Online.

