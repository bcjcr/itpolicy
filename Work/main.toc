\contentsline {section}{\numberline {1}History}{2}{section.1}%
\contentsline {section}{\numberline {2}Introduction}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}What this document is}{3}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Who can change this document}{3}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}What this document contains}{3}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}Philosophy}{3}{subsection.2.4}%
\contentsline {section}{\numberline {3}Current service provision}{4}{section.3}%
\contentsline {subsection}{\numberline {3.1}The JCR}{4}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}The College}{5}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}The University}{5}{subsection.3.3}%
\contentsline {section}{\numberline {4}Future Development}{5}{section.4}%
\contentsline {subsection}{\numberline {4.1}Short term}{5}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Long term}{6}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Aspirations}{6}{subsection.4.3}%
\contentsline {section}{\numberline {5}Matters of support and discouragement}{6}{section.5}%
\contentsline {subsection}{\numberline {5.1}Initiatives for support}{6}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Discouraged practices}{6}{subsection.5.2}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
