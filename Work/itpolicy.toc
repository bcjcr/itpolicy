\contentsline {section}{\numberline {1}History}{3}{section.1}%
\contentsline {section}{\numberline {2}Introduction}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}What this document is}{3}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Who can change this document}{4}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}What this document contains}{4}{subsection.2.3}%
\contentsline {section}{\numberline {3}Philosophy}{4}{section.3}%
\contentsline {section}{\numberline {4}Current service provision}{5}{section.4}%
\contentsline {subsection}{\numberline {4.1}The JCR}{5}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}2019 to March 2022}{6}{subsubsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.2}April 2022 - November 2022}{6}{subsubsection.4.1.2}%
\contentsline {paragraph}{\nonumberline BalliolLISTEN}{6}{paragraph*.3}%
\contentsline {paragraph}{\nonumberline BalliolGALLERY}{6}{paragraph*.5}%
\contentsline {paragraph}{\nonumberline Updated Website}{6}{paragraph*.7}%
\contentsline {paragraph}{\nonumberline BalliolCHAT}{6}{paragraph*.9}%
\contentsline {paragraph}{\nonumberline Facebook termination}{6}{paragraph*.11}%
\contentsline {subsubsection}{\numberline {4.1.3}November 2022 - January 2023}{7}{subsubsection.4.1.3}%
\contentsline {paragraph}{\nonumberline BalliolLISTEN}{7}{paragraph*.13}%
\contentsline {paragraph}{\nonumberline \color {red}BalliolGALLERY}{7}{paragraph*.15}%
\contentsline {paragraph}{\nonumberline Website}{7}{paragraph*.17}%
\contentsline {paragraph}{\nonumberline BalliolCHAT}{7}{paragraph*.19}%
\contentsline {paragraph}{\nonumberline BalliolTOOT}{7}{paragraph*.21}%
\contentsline {subsubsection}{\numberline {4.1.4}January 2023 - October 2023}{7}{subsubsection.4.1.4}%
\contentsline {paragraph}{\nonumberline BalliolCHAT}{7}{paragraph*.23}%
\contentsline {paragraph}{\nonumberline Website}{7}{paragraph*.25}%
\contentsline {paragraph}{\nonumberline Weekly Updates}{7}{paragraph*.27}%
\contentsline {paragraph}{\nonumberline Just Stop Oil (JSO) subscription}{7}{paragraph*.29}%
\contentsline {paragraph}{\nonumberline BalliolLISTEN}{8}{paragraph*.31}%
\contentsline {paragraph}{\nonumberline BalliolTOOT}{8}{paragraph*.33}%
\contentsline {paragraph}{\nonumberline Threat matrix modelling and security testing}{8}{paragraph*.35}%
\contentsline {paragraph}{\nonumberline Music Society Website}{8}{paragraph*.37}%
\contentsline {paragraph}{\nonumberline Boat Club Website}{8}{paragraph*.39}%
\contentsline {subsubsection}{\numberline {4.1.5}October 2023 - Present}{8}{subsubsection.4.1.5}%
\contentsline {paragraph}{\nonumberline BalliolCHAT}{8}{paragraph*.41}%
\contentsline {paragraph}{\nonumberline Website}{8}{paragraph*.43}%
\contentsline {paragraph}{\nonumberline Weekly Updates}{8}{paragraph*.45}%
\contentsline {paragraph}{\nonumberline \color {red}Just Stop Oil (JSO) subscription}{8}{paragraph*.47}%
\contentsline {paragraph}{\nonumberline BalliolLISTEN}{8}{paragraph*.49}%
\contentsline {paragraph}{\nonumberline BalliolTOOT}{8}{paragraph*.51}%
\contentsline {paragraph}{\nonumberline Threat matrix modelling and security testing}{8}{paragraph*.53}%
\contentsline {paragraph}{\nonumberline Music Society Website}{8}{paragraph*.55}%
\contentsline {paragraph}{\nonumberline \color {red}Boat Club Website}{9}{paragraph*.57}%
\contentsline {paragraph}{\nonumberline BalliolMob}{9}{paragraph*.59}%
\contentsline {subsection}{\numberline {4.2}The College}{9}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}The University}{9}{subsection.4.3}%
\contentsline {section}{\numberline {5}Future Development}{9}{section.5}%
\contentsline {subsection}{\numberline {5.1}Short term}{9}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Long term}{9}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Aspirations}{10}{subsection.5.3}%
\contentsline {section}{\numberline {6}Matters of support and discouragement}{10}{section.6}%
\contentsline {subsection}{\numberline {6.1}Initiatives for support}{10}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Discouraged practices}{10}{subsection.6.2}%
\contentsline {section}{\numberline {7}Termly Activity Reports}{10}{section.7}%
\contentsline {subsection}{\numberline {7.1}Hilary Term 2023 - Tobias Bretschneider}{10}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Trinity Term 2023 - Tobias Bretschneider}{11}{subsection.7.2}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
