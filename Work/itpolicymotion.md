## This JCR notes:
 
- That as of January 2022 the JCR does not have an IT policy.

--- 

## This JCR believes: 

- An IT policy would be important when;
	- planning sustainable and ethical services that the JCR should provide.
	- encouraging the College and University to alter their IT provision.
	- ensuring that the JCR meets the IT needs of its members adequately.

---

## This JCR resolves:

- That the JCR includes an IT policy in its standing policy.
- That members can influence this policy.
- That the policy acts as a best practice guideline for how the JCR should act.
- That the following two media act as a starting point for the proposed policy;
	- [https://ballioljcr.org/itpolicy.pdf](https://ballioljcr.org/itpolicy.pdf)
	- [https://ballioljcr.org/itpolicy.mp4](https://ballioljcr.org/itpolicy.mp4)

